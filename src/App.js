import "./App.css"
import {useState, useMemo} from 'react'
 

function App() {
  const [text, setText] = useState('');
  const [valid, setValid] = useState(false);
  useMemo(() => {
    isTextNumber(text)
  }, [text])
  return (
    <div className="App">
     <div className="control has-icons-right">
        <input
          className="input is-large"
          type="text"
          placeholder="Enter number..."
          value={text}
          onChange={(e) => setText(e.target.value)}

        />
        <span className="icon is-small is-right">
          <i className={valid
             ? "fas fa check" 
             : "fas fa-times"
             } />
        </span> 
      </div>
    </div>
  );

function isTextNumber(text) {
  const regex = new RegExp('^[0-9]'); 
  setValid(regex.test(text));
}
}

export default App;
