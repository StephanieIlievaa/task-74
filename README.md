# A React Task
* In this task we will create a text filled thst tells its input is a valid number.
## Objective
* Checkout the dev branch.
* Add functionality to the text field and update its right icon, depending on the number's validity.
* When implemented, merge dev branch to the master.
## Requirements
* The project starts with **npm run start**.
* Must use the **useState** to create a **text** state variable.
* Must use **useMemo** to get a boolean, telling you if the number in the input is a valid or not.
* Must update the icon`s class to fa-check if the number in the input is valid. 
## Gotchas
RegEx docs - https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
